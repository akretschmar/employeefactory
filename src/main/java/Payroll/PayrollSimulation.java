package Payroll;

/**
 *
 * @author akret
 */
public class PayrollSimulation {

    public static void main(String[] args) {
     EmployeeFactory factory = EmployeeFactory.getInstance();
     
     Employee Bob = factory.getEmployee(EmployeeType.EMPLOYEE, "Bob", 40, 22.00, 0);
     Employee Joe = factory.getEmployee(EmployeeType.MANAGER, "Joe", 40, 25.00, 100);
     
     
     System.out.println("Employee: " + Bob.getName() + "\n " + Bob.getClass().getCanonicalName()+ " Total wage is " + Bob.calculatePay());
     System.out.println("Employee: " + Joe.getName() + "\n " + Joe.getClass().getCanonicalName()+ " Total wage is " + Joe.calculatePay());
    }
    
}


package Payroll;

/**
 *
 * @author akret
 */
public class EmployeeFactory {
    public static EmployeeFactory instance;
    
    private EmployeeFactory(){}
    
    public static EmployeeFactory getInstance(){
        if(instance==null){
        instance = new EmployeeFactory();
        }
        return instance;
    }
    public Employee getEmployee(EmployeeType type, String name, double hours, double wage, double bonus){
        switch( type ){
            case EMPLOYEE : return new Employee( name, hours, wage);
            case MANAGER : return new Manager( name, hours , wage, bonus);
            
        }
        return null;
    }
}
